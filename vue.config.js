module.exports = {
  devServer: { // 默认会自动加载devServer里的配置表
    host: 'localhost', // 访问的主机
    port: 8080, // 端口
    proxy: { // 代理：访问了/a接口，实际上代理了/b，那么真正访问的是/b接口
      '/api': {
        target: 'http://mall-pre.springboot.cn', //真实接口地址
        changeOrigin: true, // 是否将主机头的原点更改为目标url的地址
        pathRewrite: { // 
          '/api': ''
        }
      }
    }
  }
}
let baseURL;
switch (process.env.NODE_ENV) {
  case 'development': // 开发
    baseURL = 'http://dev-mall-pre.springboot.cn/api';
    break;
  case 'test': // 测试
    baseURL = 'http://test-mall-pre.springboot.cn/api';
    break;
  case 'prod': // 线上
    baseURL = 'http://mall-pre.springboot.cn/api';
    break;
  default:
    baseURL = 'http://mall-pre.springboot.cn/api';
    break;
}


export default {
  baseURL
}
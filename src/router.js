import Vue from 'vue'
import Router from 'vue-router'
import Home from './pages/home'
import Login from './pages/login'
import Index from './pages/index'
import Product from './pages/product'
import Detail from './pages/detail'
import Cart from './pages/cart'
import Order from './pages/order'
import OrderList from './pages/orderList'
import OrderConfirm from './pages/orderConfirm'
import OrderPay from './pages/orderPay'
import Alipay from './pages/alipay'

Vue.use(Router); // Vue.use：加载插件的固定语法  通过vue的方式去加载这个插件

export default new Router({
  routes: [ // 配置一系列的子路由、路由的列表
    { // 刷新时默认访问这里
      path: '/', //这里的首页、产品站、商品详情共用一个大路由，这里要做一个路由的嵌套
      name: 'home',
      component: Home,
      redirect: '/index', // 重定向：默认跳到index里去
      children: [ // 子路由
        { //首页
          path: '/index',
          name: 'index',
          component: Index,
        },{ //产品站：每一件商品都有一个商品id，子路由时一个动态的路由
          path: '/product/:id',
          name: 'product',
          component: Product,
        },{ //商品详情：根据商品的id去加载内容，这也是动态的路由
          path: '/detail/:id',
          name: 'detail',
          component: Detail,
        }
      ]
    },
    { // 登录页面
      path: '/login',
      name: 'login',
      component: Login
    },
    { // 购物车
      path: '/cart',
      name: 'cart',
      component: Cart
    },
    { // 订单根组件
      path: '/order',
      name: 'order',
      component: Order,
      children: [ // （订单的）子路由
        { // 订单列表
          path: 'list',
          name: 'order-list',
          component: OrderList
        },
        { // 订单确认
          path: 'confirm',
          name: 'order-confirm',
          component: OrderConfirm
        },
        { //订单支付
          path: 'pay',
          name: 'order-pay',
          component: OrderPay
        },
        {
          path: 'alipay',
          name: 'order-alipay',
          component: Alipay
        }
      ]
    }
  ]
})
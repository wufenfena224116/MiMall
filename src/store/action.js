// 商城Vuex-actions
export default {
  // 保存用户信息
  saveUserName(context, username){
    context.commit('saveUserName', username) // 通过commit会调用mutation，把参数username传过去
  },
  saveCartCount(context, count){
    context.commit('saveCartCount', count)
  }
}
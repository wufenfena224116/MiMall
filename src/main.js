import Vue from 'vue'
import router from './router' //把写好的路由引入进来
import axios from 'axios'
import VueAxios from 'vue-axios' //把作用域对象挂载到vue实例中去，方便我们用this去调用 
import VueCookie from 'vue-cookie' //
import { Message } from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import VueLazyLoad from 'vue-lazyload'
import store from './store/index.js'
// import env from './env'

const mock = false // 定义一个mock开关
if(mock){
  require('./mock/api')
}
// 根据前端的跨域方式做调整
// axios.defaults.baseURL =  'https://www.easy-mock.com/mock/5f1e55c449c2a40963c46345/mimall' // mock地址
axios.defaults.baseURL =  '/api'
axios.defaults.timeout = 8000 // 设置8s的超时时间
// 根据环境变量获取不同的请求地址
// axios.defaults.baseURL = env.baseURL
// 接口错误拦截
axios.interceptors.response.use(function(response){
  // console.log(response)
  let res = response.data // 获取接口返回值
  let path = location.hash // 获取路径
  if (res.status == 0) { // 成功
    return res.data
  } else if (res.status == 10) { // 登陆拦截：没有登录时，接口报错，状态码为10
    if(path != '#/index'){ // 当是首页的时候不进行跳转，其他页面需要进行跳转
      window.location.href = '/#/login' // 跳转到登录页面
    }
    return Promise.reject(res) // 抛出异常
  } else { // 失败
    // alert(res.msg)
    Message.warning(res.msg)
    return Promise.reject(res) // 抛出异常
  }
})

Vue.use(VueAxios, axios)
Vue.use(VueLazyLoad, {
  loading: '/imgs/loading-svg/loading-bars.svg'
})
Vue.use(VueCookie)
Vue.config.productionTip = false

new Vue({
  store,
  router, // 如果键值都是router的话，可以简写为：router
  render: h => h(App),
}).$mount('#app')

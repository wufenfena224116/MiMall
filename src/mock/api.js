import Mock from 'mockjs' // 导入插件库
Mock.mock('/api/user/login', { // 第一个参数书是请求地址，第二个参数是返回值
  "status": 0,
  "data": {
    "id|100-700": 12,
    "username": "@cname",
    "email": "admin@51purse.com",
    "phone": null,
    "role": 0,
    "createTime": 1479048325000,
    "updateTime": 1479048325000
  }
})
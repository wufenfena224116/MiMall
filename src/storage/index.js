/**
 * Storage封装
 */
const STORAGE_KEY = 'mall';
export default {
  // 存储值：可以是任何形式的
  setItem (key, value, module_name) {
    if (module_name) {
      let val = this.getItem(module_name);
      val[key] = value;
      this.setItem(module_name, val)
    } else {
      let val = this.getStorage(); //先获取里面所有的值
      val[key] = value; // 往里面存值
      window.sessionStorage.setItem(STORAGE_KEY, JSON.stringify(val));
    }
  },
  // 获取值  
  getItem (key, module_name) { // 获取哪个值
    if (module_name) { // 获取摸一个模块下的属性user下面的userName
      let val = this.getItem(module_name);
      if (val) return val[key];
    }
    return this.getStorage()[key]; //可以获取user字段具体的信息
  },
  // 获取整个数据（获取整个浏览器的缓存信息）
  getStorage () {
    return JSON.parse(window.sessionStorage.getItem(STORAGE_KEY) || '{}'); //一开始是空的，给一个空的字符串
  },
  // 清空某一个值
  clear (key, module_name) {
    let val = this.getStorage(); // 先获取整个信息
    if (module_name) {
      if (!val[module_name]) return; //当模块不存在是返回回去
      delete val[module_name][key]; // 删除某个模块下的属性
    } else {
        delete val[key]; // 没有模块，直接干掉这个属性
    }
    window.sessionStorage.setItem(STORAGE_KEY, JSON.stringify(val)); // 删除之后，把剩余的整个值重新写进去
  }
}
